from flask import request

from redash.models import db, Organization
from redash.handlers.base import BaseResource, record_event
from redash.permissions import require_admin
from redash.settings.organization import settings as org_settings


# aes
from Crypto.Cipher import AES
import base64

def pkcs7padding(text):
    """
    明文使用PKCS7填充
    最终调用AES加密方法时，传入的是一个byte数组，要求是16的整数倍，因此需要对明文进行处理
    :param text: 待加密内容(明文)
    :return:
    """
    bs = AES.block_size  # 16
    length = len(text)
    bytes_length = len(bytes(text, encoding='utf-8'))
    # tips：utf-8编码时，英文占1个byte，而中文占3个byte
    padding_size = length if(bytes_length == length) else bytes_length
    padding = bs - padding_size % bs
    # tips：chr(padding)看与其它语言的约定，有的会使用'\0'
    padding_text = chr(padding) * padding
    return text + padding_text


def aes_encrypt(content):
    """
    AES加密
    key,iv使用同一个
    模式cbc
    填充pkcs7
    :param key: 密钥
    :param content: 加密内容
    :return:
    """    
    key = 'T1FDwKcmz3DvNui0'
    iv = 'b3r4z4p1LxMSCoO7'
    key_bytes = bytes(key, encoding='utf-8')
    iv_bytes = bytes(iv, encoding='utf-8')
    cipher = AES.new(key_bytes, AES.MODE_CBC, iv_bytes)
    # 处理明文
    content_padding = pkcs7padding(content)
    # 加密
    encrypt_bytes = cipher.encrypt(bytes(content_padding, encoding='utf-8'))
    # 重新编码
    result = str(base64.b64encode(encrypt_bytes), encoding='utf-8')
    return result



def get_settings_with_defaults(defaults, org):
    values = org.settings.get("settings", {})
    settings = {}

    for setting, default_value in defaults.items():
        current_value = values.get(setting)
        if current_value is None and default_value is None:
            continue

        if current_value is None:
            settings[setting] = default_value
        else:
            settings[setting] = current_value

    settings["auth_google_apps_domains"] = org.google_apps_domains

    return settings


class OrganizationSettings(BaseResource):
    @require_admin
    def get(self):
        settings = get_settings_with_defaults(org_settings, self.current_org)

        return {"settings": settings}

    @require_admin
    def post(self):
        new_values = request.json

        if self.current_org.settings.get("settings") is None:
            self.current_org.settings["settings"] = {}

        previous_values = {}
        for k, v in new_values.items():
            if k == "auth_google_apps_domains":
                previous_values[k] = self.current_org.google_apps_domains
                self.current_org.settings[Organization.SETTING_GOOGLE_APPS_DOMAINS] = v
            else:
                previous_values[k] = self.current_org.get_setting(
                    k, raise_on_missing=False
                )
                self.current_org.set_setting(k, v)

        db.session.add(self.current_org)
        db.session.commit()

        self.record_event(
            {
                "action": "edit",
                "object_id": self.current_org.id,
                "object_type": "settings",
                "new_values": new_values,
                "previous_values": previous_values,
            }
        )

        settings = get_settings_with_defaults(org_settings, self.current_org)

        return {"settings": settings}
