#!/bin/bash
source /etc/profile

# python3虚拟环境安装
yum -y install python3 gcc-c++ python3-devel python-devel postgresql-devel postgresql
pip3 install virtualenvwrapper

echo "
# redash config
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
" >> /etc/profile

source /etc/profile

mkvirtualenv --python=/usr/bin/python3 py3_redash

# 安装依赖包
pip3 install -r requirements.txt


# redash_home=$(pwd)
# virtualenv_home=$(pwd)/.virtualenvs/py3_redash/
# virtualenv_bin=$(pwd)/.virtualenvs/py3_redash/bin/python3

# # 替换uwsgi.ini配置
# sed -i '/^chdir=/c${redash_home}' uwsgi.ini
# sed -i '/^virtualenv_home=/c${virtualenv_home}' uwsgi.ini


# # 替换supervisord
# sed -i '/^virtualenv_bin/c${virtualenv_bin}' supervisord.conf
# sed -i '/^redash_home/c${redash_home}' supervisord.conf


echo "安装完成"
echo "请手工修改如下2个根文件中的路径配置：uwsgi.ini和supervisord.conf"

