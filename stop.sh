#!/usr/bin/env bash

`ps -ef | grep redash | grep -v grep | awk '{print $2}' | xargs kill -9`
`ps -ef | grep uwsgi | grep -v grep | awk '{print $2}' | xargs kill -9`
`ps -ef | grep supervisord | grep -v grep | awk '{print $2}' | xargs kill -9`