--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.2

-- Started on 2021-12-22 15:51:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 244 (class 1255 OID 869256)
-- Name: queries_search_vector_update(); Type: FUNCTION; Schema: public; Owner: redash
--

CREATE FUNCTION public.queries_search_vector_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                NEW.search_vector = ((setweight(to_tsvector('pg_catalog.simple', regexp_replace(coalesce(CAST(NEW.id AS TEXT), ''), '[-@.]', ' ', 'g')), 'B') || setweight(to_tsvector('pg_catalog.simple', regexp_replace(coalesce(NEW.name, ''), '[-@.]', ' ', 'g')), 'A')) || setweight(to_tsvector('pg_catalog.simple', regexp_replace(coalesce(NEW.description, ''), '[-@.]', ' ', 'g')), 'C')) || setweight(to_tsvector('pg_catalog.simple', regexp_replace(coalesce(NEW.query, ''), '[-@.]', ' ', 'g')), 'D');
                RETURN NEW;
            END
            $$;


ALTER FUNCTION public.queries_search_vector_update() OWNER TO redash;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 869257)
-- Name: access_permissions; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.access_permissions (
    object_type character varying(255) NOT NULL,
    object_id integer NOT NULL,
    id integer NOT NULL,
    access_type character varying(255) NOT NULL,
    grantor_id integer NOT NULL,
    grantee_id integer NOT NULL
);


ALTER TABLE public.access_permissions OWNER TO redash;

--
-- TOC entry 201 (class 1259 OID 869263)
-- Name: access_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.access_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_permissions_id_seq OWNER TO redash;

--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 201
-- Name: access_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.access_permissions_id_seq OWNED BY public.access_permissions.id;


--
-- TOC entry 202 (class 1259 OID 869265)
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO redash;

--
-- TOC entry 203 (class 1259 OID 869268)
-- Name: alert_subscriptions; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.alert_subscriptions (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    user_id integer NOT NULL,
    destination_id integer,
    alert_id integer NOT NULL
);


ALTER TABLE public.alert_subscriptions OWNER TO redash;

--
-- TOC entry 204 (class 1259 OID 869271)
-- Name: alert_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.alert_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alert_subscriptions_id_seq OWNER TO redash;

--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 204
-- Name: alert_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.alert_subscriptions_id_seq OWNED BY public.alert_subscriptions.id;


--
-- TOC entry 205 (class 1259 OID 869273)
-- Name: alerts; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.alerts (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    query_id integer NOT NULL,
    user_id integer NOT NULL,
    options text NOT NULL,
    state character varying(255) NOT NULL,
    last_triggered_at timestamp with time zone,
    rearm integer
);


ALTER TABLE public.alerts OWNER TO redash;

--
-- TOC entry 206 (class 1259 OID 869279)
-- Name: alerts_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.alerts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alerts_id_seq OWNER TO redash;

--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 206
-- Name: alerts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.alerts_id_seq OWNED BY public.alerts.id;


--
-- TOC entry 207 (class 1259 OID 869281)
-- Name: api_keys; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.api_keys (
    object_type character varying(255) NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    org_id integer NOT NULL,
    api_key character varying(255) NOT NULL,
    active boolean NOT NULL,
    object_id integer NOT NULL,
    created_by_id integer
);


ALTER TABLE public.api_keys OWNER TO redash;

--
-- TOC entry 208 (class 1259 OID 869287)
-- Name: api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.api_keys_id_seq OWNER TO redash;

--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 208
-- Name: api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.api_keys_id_seq OWNED BY public.api_keys.id;


--
-- TOC entry 209 (class 1259 OID 869289)
-- Name: changes; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.changes (
    object_type character varying(255) NOT NULL,
    id integer NOT NULL,
    object_id integer NOT NULL,
    object_version integer NOT NULL,
    user_id integer NOT NULL,
    change text NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.changes OWNER TO redash;

--
-- TOC entry 210 (class 1259 OID 869295)
-- Name: changes_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.changes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.changes_id_seq OWNER TO redash;

--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 210
-- Name: changes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.changes_id_seq OWNED BY public.changes.id;


--
-- TOC entry 211 (class 1259 OID 869297)
-- Name: dashboard_click_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.dashboard_click_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dashboard_click_seq OWNER TO redash;

--
-- TOC entry 212 (class 1259 OID 869299)
-- Name: dashboard_clicks; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.dashboard_clicks (
    id integer DEFAULT nextval('public.dashboard_click_seq'::regclass) NOT NULL,
    dashboard_id integer NOT NULL,
    user_id integer NOT NULL,
    create_time timestamp with time zone DEFAULT now() NOT NULL,
    click_type integer
);


ALTER TABLE public.dashboard_clicks OWNER TO redash;

--
-- TOC entry 213 (class 1259 OID 869304)
-- Name: dashboard_groups; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.dashboard_groups (
    id integer NOT NULL,
    dashboard_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.dashboard_groups OWNER TO redash;

--
-- TOC entry 214 (class 1259 OID 869307)
-- Name: dashboard_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.dashboard_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.dashboard_groups_id_seq OWNER TO redash;

--
-- TOC entry 215 (class 1259 OID 869309)
-- Name: dashboard_groups_id_seq1; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.dashboard_groups_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dashboard_groups_id_seq1 OWNER TO redash;

--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 215
-- Name: dashboard_groups_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.dashboard_groups_id_seq1 OWNED BY public.dashboard_groups.id;


--
-- TOC entry 216 (class 1259 OID 869311)
-- Name: dashboards; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.dashboards (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    version integer NOT NULL,
    org_id integer NOT NULL,
    slug character varying(140) NOT NULL,
    name character varying(100) NOT NULL,
    user_id integer NOT NULL,
    layout text NOT NULL,
    dashboard_filters_enabled boolean NOT NULL,
    is_archived boolean NOT NULL,
    is_draft boolean NOT NULL,
    tags character varying[]
);


ALTER TABLE public.dashboards OWNER TO redash;

--
-- TOC entry 217 (class 1259 OID 869317)
-- Name: dashboards_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.dashboards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dashboards_id_seq OWNER TO redash;

--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 217
-- Name: dashboards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.dashboards_id_seq OWNED BY public.dashboards.id;


--
-- TOC entry 218 (class 1259 OID 869319)
-- Name: data_source_groups; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.data_source_groups (
    id integer NOT NULL,
    data_source_id integer NOT NULL,
    group_id integer NOT NULL,
    view_only boolean NOT NULL
);


ALTER TABLE public.data_source_groups OWNER TO redash;

--
-- TOC entry 219 (class 1259 OID 869322)
-- Name: data_source_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.data_source_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_source_groups_id_seq OWNER TO redash;

--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 219
-- Name: data_source_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.data_source_groups_id_seq OWNED BY public.data_source_groups.id;


--
-- TOC entry 220 (class 1259 OID 869324)
-- Name: data_sources; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.data_sources (
    id integer NOT NULL,
    org_id integer NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    encrypted_options bytea NOT NULL,
    queue_name character varying(255) NOT NULL,
    scheduled_queue_name character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.data_sources OWNER TO redash;

--
-- TOC entry 221 (class 1259 OID 869330)
-- Name: data_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.data_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_sources_id_seq OWNER TO redash;

--
-- TOC entry 3450 (class 0 OID 0)
-- Dependencies: 221
-- Name: data_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.data_sources_id_seq OWNED BY public.data_sources.id;


--
-- TOC entry 222 (class 1259 OID 869332)
-- Name: events; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.events (
    id integer NOT NULL,
    org_id integer NOT NULL,
    user_id integer,
    action character varying(255) NOT NULL,
    object_type character varying(255) NOT NULL,
    object_id character varying(255),
    additional_properties text,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.events OWNER TO redash;

--
-- TOC entry 223 (class 1259 OID 869338)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO redash;

--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 223
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- TOC entry 224 (class 1259 OID 869340)
-- Name: favorites; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.favorites (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    org_id integer NOT NULL,
    object_type character varying(255) NOT NULL,
    object_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.favorites OWNER TO redash;

--
-- TOC entry 225 (class 1259 OID 869343)
-- Name: favorites_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.favorites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.favorites_id_seq OWNER TO redash;

--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 225
-- Name: favorites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.favorites_id_seq OWNED BY public.favorites.id;


--
-- TOC entry 226 (class 1259 OID 869345)
-- Name: groups; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    org_id integer NOT NULL,
    type character varying(255) NOT NULL,
    name character varying(100) NOT NULL,
    permissions character varying(255)[] NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.groups OWNER TO redash;

--
-- TOC entry 227 (class 1259 OID 869351)
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO redash;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 227
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- TOC entry 228 (class 1259 OID 869353)
-- Name: notification_destinations; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.notification_destinations (
    id integer NOT NULL,
    org_id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    options text NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.notification_destinations OWNER TO redash;

--
-- TOC entry 229 (class 1259 OID 869359)
-- Name: notification_destinations_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.notification_destinations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_destinations_id_seq OWNER TO redash;

--
-- TOC entry 3454 (class 0 OID 0)
-- Dependencies: 229
-- Name: notification_destinations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.notification_destinations_id_seq OWNED BY public.notification_destinations.id;


--
-- TOC entry 230 (class 1259 OID 869361)
-- Name: organizations; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.organizations (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    settings text NOT NULL
);


ALTER TABLE public.organizations OWNER TO redash;

--
-- TOC entry 231 (class 1259 OID 869367)
-- Name: organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.organizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizations_id_seq OWNER TO redash;

--
-- TOC entry 3455 (class 0 OID 0)
-- Dependencies: 231
-- Name: organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.organizations_id_seq OWNED BY public.organizations.id;


--
-- TOC entry 232 (class 1259 OID 869369)
-- Name: queries; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.queries (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    version integer NOT NULL,
    org_id integer NOT NULL,
    data_source_id integer,
    latest_query_data_id integer,
    name character varying(255) NOT NULL,
    description character varying(4096),
    query text NOT NULL,
    query_hash character varying(32) NOT NULL,
    api_key character varying(40) NOT NULL,
    user_id integer NOT NULL,
    last_modified_by_id integer,
    is_archived boolean NOT NULL,
    is_draft boolean NOT NULL,
    schedule text,
    schedule_failures integer NOT NULL,
    options text NOT NULL,
    search_vector tsvector,
    tags character varying[]
);


ALTER TABLE public.queries OWNER TO redash;

--
-- TOC entry 233 (class 1259 OID 869375)
-- Name: queries_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queries_id_seq OWNER TO redash;

--
-- TOC entry 3456 (class 0 OID 0)
-- Dependencies: 233
-- Name: queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.queries_id_seq OWNED BY public.queries.id;


--
-- TOC entry 234 (class 1259 OID 869377)
-- Name: query_results; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.query_results (
    id integer NOT NULL,
    org_id integer NOT NULL,
    data_source_id integer NOT NULL,
    query_hash character varying(32) NOT NULL,
    query text NOT NULL,
    data text NOT NULL,
    runtime double precision NOT NULL,
    retrieved_at timestamp with time zone NOT NULL
);


ALTER TABLE public.query_results OWNER TO redash;

--
-- TOC entry 235 (class 1259 OID 869383)
-- Name: query_results_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.query_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.query_results_id_seq OWNER TO redash;

--
-- TOC entry 3457 (class 0 OID 0)
-- Dependencies: 235
-- Name: query_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.query_results_id_seq OWNED BY public.query_results.id;


--
-- TOC entry 236 (class 1259 OID 869385)
-- Name: query_snippets; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.query_snippets (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    org_id integer NOT NULL,
    trigger character varying(255) NOT NULL,
    description text NOT NULL,
    user_id integer NOT NULL,
    snippet text NOT NULL
);


ALTER TABLE public.query_snippets OWNER TO redash;

--
-- TOC entry 237 (class 1259 OID 869391)
-- Name: query_snippets_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.query_snippets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.query_snippets_id_seq OWNER TO redash;

--
-- TOC entry 3458 (class 0 OID 0)
-- Dependencies: 237
-- Name: query_snippets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.query_snippets_id_seq OWNED BY public.query_snippets.id;


--
-- TOC entry 238 (class 1259 OID 869393)
-- Name: users; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.users (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    org_id integer NOT NULL,
    name character varying(320) NOT NULL,
    email character varying(255) NOT NULL,
    profile_image_url character varying(320),
    password_hash character varying(128),
    groups integer[],
    api_key character varying(40) NOT NULL,
    disabled_at timestamp with time zone,
    details json DEFAULT '{}'::json
);


ALTER TABLE public.users OWNER TO redash;

--
-- TOC entry 239 (class 1259 OID 869400)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO redash;

--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 239
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 240 (class 1259 OID 869402)
-- Name: visualizations; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.visualizations (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    type character varying(100) NOT NULL,
    query_id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(4096),
    options text NOT NULL
);


ALTER TABLE public.visualizations OWNER TO redash;

--
-- TOC entry 241 (class 1259 OID 869408)
-- Name: visualizations_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.visualizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visualizations_id_seq OWNER TO redash;

--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 241
-- Name: visualizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.visualizations_id_seq OWNED BY public.visualizations.id;


--
-- TOC entry 242 (class 1259 OID 869410)
-- Name: widgets; Type: TABLE; Schema: public; Owner: redash
--

CREATE TABLE public.widgets (
    updated_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    visualization_id integer,
    text text,
    width integer NOT NULL,
    options text NOT NULL,
    dashboard_id integer NOT NULL
);


ALTER TABLE public.widgets OWNER TO redash;

--
-- TOC entry 243 (class 1259 OID 869416)
-- Name: widgets_id_seq; Type: SEQUENCE; Schema: public; Owner: redash
--

CREATE SEQUENCE public.widgets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.widgets_id_seq OWNER TO redash;

--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 243
-- Name: widgets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redash
--

ALTER SEQUENCE public.widgets_id_seq OWNED BY public.widgets.id;


--
-- TOC entry 3131 (class 2604 OID 869418)
-- Name: access_permissions id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.access_permissions ALTER COLUMN id SET DEFAULT nextval('public.access_permissions_id_seq'::regclass);


--
-- TOC entry 3132 (class 2604 OID 869419)
-- Name: alert_subscriptions id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alert_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.alert_subscriptions_id_seq'::regclass);


--
-- TOC entry 3133 (class 2604 OID 869420)
-- Name: alerts id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alerts ALTER COLUMN id SET DEFAULT nextval('public.alerts_id_seq'::regclass);


--
-- TOC entry 3134 (class 2604 OID 869421)
-- Name: api_keys id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.api_keys ALTER COLUMN id SET DEFAULT nextval('public.api_keys_id_seq'::regclass);


--
-- TOC entry 3135 (class 2604 OID 869422)
-- Name: changes id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.changes ALTER COLUMN id SET DEFAULT nextval('public.changes_id_seq'::regclass);


--
-- TOC entry 3138 (class 2604 OID 869423)
-- Name: dashboard_groups id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboard_groups ALTER COLUMN id SET DEFAULT nextval('public.dashboard_groups_id_seq1'::regclass);


--
-- TOC entry 3139 (class 2604 OID 869424)
-- Name: dashboards id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboards ALTER COLUMN id SET DEFAULT nextval('public.dashboards_id_seq'::regclass);


--
-- TOC entry 3140 (class 2604 OID 869425)
-- Name: data_source_groups id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_source_groups ALTER COLUMN id SET DEFAULT nextval('public.data_source_groups_id_seq'::regclass);


--
-- TOC entry 3141 (class 2604 OID 869426)
-- Name: data_sources id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_sources ALTER COLUMN id SET DEFAULT nextval('public.data_sources_id_seq'::regclass);


--
-- TOC entry 3142 (class 2604 OID 869427)
-- Name: events id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- TOC entry 3143 (class 2604 OID 869428)
-- Name: favorites id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.favorites ALTER COLUMN id SET DEFAULT nextval('public.favorites_id_seq'::regclass);


--
-- TOC entry 3144 (class 2604 OID 869429)
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- TOC entry 3145 (class 2604 OID 869430)
-- Name: notification_destinations id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.notification_destinations ALTER COLUMN id SET DEFAULT nextval('public.notification_destinations_id_seq'::regclass);


--
-- TOC entry 3146 (class 2604 OID 869431)
-- Name: organizations id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.organizations ALTER COLUMN id SET DEFAULT nextval('public.organizations_id_seq'::regclass);


--
-- TOC entry 3147 (class 2604 OID 869432)
-- Name: queries id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries ALTER COLUMN id SET DEFAULT nextval('public.queries_id_seq'::regclass);


--
-- TOC entry 3148 (class 2604 OID 869433)
-- Name: query_results id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_results ALTER COLUMN id SET DEFAULT nextval('public.query_results_id_seq'::regclass);


--
-- TOC entry 3149 (class 2604 OID 869434)
-- Name: query_snippets id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_snippets ALTER COLUMN id SET DEFAULT nextval('public.query_snippets_id_seq'::regclass);


--
-- TOC entry 3151 (class 2604 OID 869435)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3152 (class 2604 OID 869436)
-- Name: visualizations id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.visualizations ALTER COLUMN id SET DEFAULT nextval('public.visualizations_id_seq'::regclass);


--
-- TOC entry 3153 (class 2604 OID 869437)
-- Name: widgets id; Type: DEFAULT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.widgets ALTER COLUMN id SET DEFAULT nextval('public.widgets_id_seq'::regclass);


--
-- TOC entry 3393 (class 0 OID 869257)
-- Dependencies: 200
-- Data for Name: access_permissions; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.access_permissions (object_type, object_id, id, access_type, grantor_id, grantee_id) FROM stdin;
\.


--
-- TOC entry 3395 (class 0 OID 869265)
-- Dependencies: 202
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.alembic_version (version_num) FROM stdin;
\.


--
-- TOC entry 3396 (class 0 OID 869268)
-- Dependencies: 203
-- Data for Name: alert_subscriptions; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.alert_subscriptions (updated_at, created_at, id, user_id, destination_id, alert_id) FROM stdin;
\.


--
-- TOC entry 3398 (class 0 OID 869273)
-- Dependencies: 205
-- Data for Name: alerts; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.alerts (updated_at, created_at, id, name, query_id, user_id, options, state, last_triggered_at, rearm) FROM stdin;
\.


--
-- TOC entry 3400 (class 0 OID 869281)
-- Dependencies: 207
-- Data for Name: api_keys; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.api_keys (object_type, updated_at, created_at, id, org_id, api_key, active, object_id, created_by_id) FROM stdin;
\.


--
-- TOC entry 3402 (class 0 OID 869289)
-- Dependencies: 209
-- Data for Name: changes; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.changes (object_type, id, object_id, object_version, user_id, change, created_at) FROM stdin;
\.


--
-- TOC entry 3405 (class 0 OID 869299)
-- Dependencies: 212
-- Data for Name: dashboard_clicks; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.dashboard_clicks (id, dashboard_id, user_id, create_time, click_type) FROM stdin;
\.


--
-- TOC entry 3406 (class 0 OID 869304)
-- Dependencies: 213
-- Data for Name: dashboard_groups; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.dashboard_groups (id, dashboard_id, group_id) FROM stdin;
\.


--
-- TOC entry 3409 (class 0 OID 869311)
-- Dependencies: 216
-- Data for Name: dashboards; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.dashboards (updated_at, created_at, id, version, org_id, slug, name, user_id, layout, dashboard_filters_enabled, is_archived, is_draft, tags) FROM stdin;
\.


--
-- TOC entry 3411 (class 0 OID 869319)
-- Dependencies: 218
-- Data for Name: data_source_groups; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.data_source_groups (id, data_source_id, group_id, view_only) FROM stdin;
\.


--
-- TOC entry 3413 (class 0 OID 869324)
-- Dependencies: 220
-- Data for Name: data_sources; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.data_sources (id, org_id, name, type, encrypted_options, queue_name, scheduled_queue_name, created_at) FROM stdin;
\.


--
-- TOC entry 3415 (class 0 OID 869332)
-- Dependencies: 222
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.events (id, org_id, user_id, action, object_type, object_id, additional_properties, created_at) FROM stdin;
1	1	3	login	redash	\N	{"user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:26+08
2	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:28+08
3	1	3	load_favorites	query	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:28+08
4	1	3	load_favorites	dashboard	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:28+08
5	1	3	view	page	personal_homepage	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:13.053+08
6	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:34+08
7	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:35+08
8	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:35+08
9	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:37+08
10	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:39+08
11	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:40+08
12	1	3	view	group	24	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:45+08
13	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:47+08
14	1	3	view	group	23	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:48+08
15	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:49+08
16	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:50+08
17	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:51+08
18	1	3	view	group	70	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:53+08
19	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:54+08
20	1	3	view	group	69	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:56+08
21	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:57+08
22	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:47:59+08
23	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:02+08
24	1	3	list	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:02+08
25	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:02+08
26	1	3	list	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:03+08
27	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:04+08
28	1	3	view	group	71	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:06+08
29	1	3	view	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:07+08
30	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:07+08
31	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:09+08
32	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:11+08
33	1	3	view	group	24	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:14+08
34	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:16+08
35	1	3	view	group	23	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:17+08
36	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:20+08
37	1	3	view	group	70	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:22+08
38	1	3	list	user	\N	{"pending": null, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:24+08
39	1	3	add_member	group	70	{"member_id": 3, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:26+08
40	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:29+08
41	1	3	view	group	70	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:30+08
42	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:34+08
43	1	3	view	group	69	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:37+08
44	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:56+08
45	1	3	view	group	70	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:58+08
46	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:00+08
47	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:02+08
48	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:03+08
49	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:04+08
50	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:05+08
51	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:06+08
52	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:07+08
53	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:08+08
54	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:09+08
55	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:10+08
56	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:10+08
57	1	3	list	destination	admin/destinations	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:11+08
58	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:12+08
59	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:13+08
60	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:13+08
61	1	3	list	destination	admin/destinations	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:14+08
62	1	3	list	query_snippet	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:14+08
63	1	3	view	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:15+08
64	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:15+08
65	1	3	view	page	org_settings	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:48:59.786+08
66	1	3	view	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:17+08
67	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:17+08
68	1	3	view	page	org_settings	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:00.959+08
69	1	3	regnerate_api_key	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:21+08
70	1	3	view	page	org_settings	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:09.52+08
71	1	3	list	query_snippet	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:26+08
72	1	3	list	destination	admin/destinations	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:26+08
73	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:27+08
74	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:28+08
75	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:28+08
76	1	3	view	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:34+08
77	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:35+08
78	1	3	list	alert	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:40+08
79	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:41+08
80	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:43+08
81	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:44+08
82	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:45+08
83	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:46+08
84	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:47+08
85	1	3	search	dashboard	\N	{"term": "123", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:52+08
86	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:54+08
87	1	3	search	query	\N	{"term": "1", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:55+08
88	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:49:56+08
89	1	3	list	datasource	admin/data_sources	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:02+08
90	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:05+08
91	1	3	view	user	3	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:07+08
92	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:07+08
93	1	3	login	redash	\N	{"user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:20+08
94	1	3	edit	user	3	{"updated_fields": [], "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:20+08
95	1	3	list	user	\N	{"pending": false, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:24+08
96	1	3	login	redash	\N	{"user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:35+08
97	1	3	list	group	groups	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:36+08
98	1	3	load_favorites	dashboard	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:36+08
99	1	3	load_favorites	query	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:36+08
100	1	3	view	page	personal_homepage	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:21.296+08
101	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:42+08
102	1	3	load_favorites	query	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:44+08
103	1	3	load_favorites	dashboard	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:44+08
104	1	3	view	page	personal_homepage	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:28.687+08
105	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:46+08
106	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:47+08
107	1	3	list	query	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:48+08
108	1	3	list	dashboard	\N	{"user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:49+08
109	1	3	load_favorites	query	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:49+08
110	1	3	load_favorites	dashboard	\N	{"params": {"q": null, "tags": [], "page": 1}, "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:49+08
111	1	3	view	page	personal_homepage	{"screen_resolution": "2560x1440", "user_name": "admin", "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36", "ip": "192.168.192.22"}	2021-12-22 07:50:34.131+08
\.


--
-- TOC entry 3417 (class 0 OID 869340)
-- Dependencies: 224
-- Data for Name: favorites; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.favorites (updated_at, created_at, id, org_id, object_type, object_id, user_id) FROM stdin;
\.


--
-- TOC entry 3419 (class 0 OID 869345)
-- Dependencies: 226
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.groups (id, org_id, type, name, permissions, created_at) FROM stdin;
24	1	builtin	default	{view_query,view_source,execute_query,list_users,schedule_query,list_dashboards,list_alerts,list_data_sources}	2021-01-06 18:49:55.36+08
23	1	builtin	admin	{admin,edit_query,create_query,create_dashboard,super_admin,edit_dashboard}	2021-01-06 18:49:55.36+08
70	1	builtin	报表	{view_query,view_source,execute_query,list_users,schedule_query,list_dashboards,list_alerts,list_data_sources}	2021-10-11 17:36:11.472+08
71	1	builtin	下载	{view_query,view_source,execute_query,list_users,schedule_query,list_dashboards,list_alerts,list_data_sources}	2021-10-22 11:34:24.113+08
69	1	builtin	查询	{view_query,view_source,execute_query,list_users,schedule_query,list_dashboards,list_alerts,list_data_sources,edit_query,create_query}	2021-10-11 14:56:58.81+08
\.


--
-- TOC entry 3421 (class 0 OID 869353)
-- Dependencies: 228
-- Data for Name: notification_destinations; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.notification_destinations (id, org_id, user_id, name, type, options, created_at) FROM stdin;
\.


--
-- TOC entry 3423 (class 0 OID 869361)
-- Dependencies: 230
-- Data for Name: organizations; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.organizations (updated_at, created_at, id, name, slug, settings) FROM stdin;
2021-12-21 14:15:51.685+08	2021-01-06 17:28:52.462+08	1	default	default	{"settings": {"beacon_consent": false, "auth_password_login_enabled": true, "auth_saml_enabled": false, "auth_saml_type": "", "auth_saml_entity_id": "", "auth_saml_metadata_url": "", "auth_saml_nameid_format": "", "auth_saml_sso_url": "", "auth_saml_x509_cert": "", "date_format": "YYYY-MM-DD", "time_format": "HH:mm:ss", "integer_format": "0,0", "float_format": "0,0.00", "multi_byte_search_enabled": true, "auth_jwt_login_enabled": false, "auth_jwt_auth_issuer": "", "auth_jwt_auth_public_certs_url": "", "auth_jwt_auth_audience": "", "auth_jwt_auth_algorithms": ["HS256", "RS256", "ES256"], "auth_jwt_auth_cookie_name": "", "auth_jwt_auth_header_name": "", "feature_show_permissions_control": true, "send_email_on_failed_scheduled_queries": true, "hide_plotly_mode_bar": false, "disable_public_urls": false}, "google_apps_domains": []}
\.


--
-- TOC entry 3425 (class 0 OID 869369)
-- Dependencies: 232
-- Data for Name: queries; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.queries (updated_at, created_at, id, version, org_id, data_source_id, latest_query_data_id, name, description, query, query_hash, api_key, user_id, last_modified_by_id, is_archived, is_draft, schedule, schedule_failures, options, search_vector, tags) FROM stdin;
\.


--
-- TOC entry 3427 (class 0 OID 869377)
-- Dependencies: 234
-- Data for Name: query_results; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.query_results (id, org_id, data_source_id, query_hash, query, data, runtime, retrieved_at) FROM stdin;
\.


--
-- TOC entry 3429 (class 0 OID 869385)
-- Dependencies: 236
-- Data for Name: query_snippets; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.query_snippets (updated_at, created_at, id, org_id, trigger, description, user_id, snippet) FROM stdin;
\.


--
-- TOC entry 3431 (class 0 OID 869393)
-- Dependencies: 238
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.users (updated_at, created_at, id, org_id, name, email, profile_image_url, password_hash, groups, api_key, disabled_at, details) FROM stdin;
2021-12-22 15:50:38.794188+08	2021-01-06 18:49:59.659+08	3	1	admin	admin@qq.com	\N	$6$rounds=656000$GFM682rfFT/Z/i6h$Vk6aZXDarv1WYk1JjnTjCKOwS8p5y.CjlxrAzd.q0jTq9uaHaC/czWeOUlDMBDPVfh7i.2htPk79dYafVBBal.	{23,24,71,70}	aD1Qg91GmeEJs3zg4CddwStJ3G8HLWm0dBo301zo	\N	{"active_at": "2021-12-22T07:50:50Z"}
\.


--
-- TOC entry 3433 (class 0 OID 869402)
-- Dependencies: 240
-- Data for Name: visualizations; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.visualizations (updated_at, created_at, id, type, query_id, name, description, options) FROM stdin;
\.


--
-- TOC entry 3435 (class 0 OID 869410)
-- Dependencies: 242
-- Data for Name: widgets; Type: TABLE DATA; Schema: public; Owner: redash
--

COPY public.widgets (updated_at, created_at, id, visualization_id, text, width, options, dashboard_id) FROM stdin;
\.


--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 201
-- Name: access_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.access_permissions_id_seq', 1, false);


--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 204
-- Name: alert_subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.alert_subscriptions_id_seq', 1, false);


--
-- TOC entry 3464 (class 0 OID 0)
-- Dependencies: 206
-- Name: alerts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.alerts_id_seq', 1, false);


--
-- TOC entry 3465 (class 0 OID 0)
-- Dependencies: 208
-- Name: api_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.api_keys_id_seq', 1, false);


--
-- TOC entry 3466 (class 0 OID 0)
-- Dependencies: 210
-- Name: changes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.changes_id_seq', 1, false);


--
-- TOC entry 3467 (class 0 OID 0)
-- Dependencies: 211
-- Name: dashboard_click_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.dashboard_click_seq', 1, false);


--
-- TOC entry 3468 (class 0 OID 0)
-- Dependencies: 214
-- Name: dashboard_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.dashboard_groups_id_seq', 1, false);


--
-- TOC entry 3469 (class 0 OID 0)
-- Dependencies: 215
-- Name: dashboard_groups_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.dashboard_groups_id_seq1', 1, false);


--
-- TOC entry 3470 (class 0 OID 0)
-- Dependencies: 217
-- Name: dashboards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.dashboards_id_seq', 1, false);


--
-- TOC entry 3471 (class 0 OID 0)
-- Dependencies: 219
-- Name: data_source_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.data_source_groups_id_seq', 1, false);


--
-- TOC entry 3472 (class 0 OID 0)
-- Dependencies: 221
-- Name: data_sources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.data_sources_id_seq', 1, false);


--
-- TOC entry 3473 (class 0 OID 0)
-- Dependencies: 223
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.events_id_seq', 111, true);


--
-- TOC entry 3474 (class 0 OID 0)
-- Dependencies: 225
-- Name: favorites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.favorites_id_seq', 1, false);


--
-- TOC entry 3475 (class 0 OID 0)
-- Dependencies: 227
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.groups_id_seq', 1, false);


--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 229
-- Name: notification_destinations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.notification_destinations_id_seq', 1, false);


--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 231
-- Name: organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.organizations_id_seq', 1, false);


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 233
-- Name: queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.queries_id_seq', 1, false);


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 235
-- Name: query_results_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.query_results_id_seq', 1, false);


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 237
-- Name: query_snippets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.query_snippets_id_seq', 1, false);


--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 239
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 241
-- Name: visualizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.visualizations_id_seq', 1, false);


--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 243
-- Name: widgets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redash
--

SELECT pg_catalog.setval('public.widgets_id_seq', 1, false);


--
-- TOC entry 3155 (class 2606 OID 869439)
-- Name: access_permissions access_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.access_permissions
    ADD CONSTRAINT access_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3157 (class 2606 OID 869441)
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- TOC entry 3160 (class 2606 OID 869443)
-- Name: alert_subscriptions alert_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alert_subscriptions
    ADD CONSTRAINT alert_subscriptions_pkey PRIMARY KEY (id);


--
-- TOC entry 3162 (class 2606 OID 869445)
-- Name: alerts alerts_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alerts
    ADD CONSTRAINT alerts_pkey PRIMARY KEY (id);


--
-- TOC entry 3165 (class 2606 OID 869447)
-- Name: api_keys api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT api_keys_pkey PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 869449)
-- Name: changes changes_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_pkey PRIMARY KEY (id);


--
-- TOC entry 3170 (class 2606 OID 869451)
-- Name: dashboard_clicks dashboard_clicks_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboard_clicks
    ADD CONSTRAINT dashboard_clicks_pkey PRIMARY KEY (id);


--
-- TOC entry 3177 (class 2606 OID 869453)
-- Name: dashboard_groups dashboard_groups_id_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboard_groups
    ADD CONSTRAINT dashboard_groups_id_pkey PRIMARY KEY (id);


--
-- TOC entry 3179 (class 2606 OID 869455)
-- Name: dashboards dashboards_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboards
    ADD CONSTRAINT dashboards_pkey PRIMARY KEY (id);


--
-- TOC entry 3184 (class 2606 OID 869457)
-- Name: data_source_groups data_source_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_source_groups
    ADD CONSTRAINT data_source_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3187 (class 2606 OID 869459)
-- Name: data_sources data_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_sources
    ADD CONSTRAINT data_sources_pkey PRIMARY KEY (id);


--
-- TOC entry 3189 (class 2606 OID 869461)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 3191 (class 2606 OID 869463)
-- Name: favorites favorites_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.favorites
    ADD CONSTRAINT favorites_pkey PRIMARY KEY (id);


--
-- TOC entry 3195 (class 2606 OID 869465)
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3198 (class 2606 OID 869467)
-- Name: notification_destinations notification_destinations_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.notification_destinations
    ADD CONSTRAINT notification_destinations_pkey PRIMARY KEY (id);


--
-- TOC entry 3200 (class 2606 OID 869469)
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);


--
-- TOC entry 3202 (class 2606 OID 869471)
-- Name: organizations organizations_slug_key; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_slug_key UNIQUE (slug);


--
-- TOC entry 3207 (class 2606 OID 869473)
-- Name: queries queries_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_pkey PRIMARY KEY (id);


--
-- TOC entry 3210 (class 2606 OID 869475)
-- Name: query_results query_results_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_results
    ADD CONSTRAINT query_results_pkey PRIMARY KEY (id);


--
-- TOC entry 3212 (class 2606 OID 869477)
-- Name: query_snippets query_snippets_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_snippets
    ADD CONSTRAINT query_snippets_pkey PRIMARY KEY (id);


--
-- TOC entry 3214 (class 2606 OID 869479)
-- Name: query_snippets query_snippets_trigger_key; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_snippets
    ADD CONSTRAINT query_snippets_trigger_key UNIQUE (trigger);


--
-- TOC entry 3193 (class 2606 OID 869481)
-- Name: favorites unique_favorite; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.favorites
    ADD CONSTRAINT unique_favorite UNIQUE (object_type, object_id, user_id);


--
-- TOC entry 3216 (class 2606 OID 869483)
-- Name: users users_api_key_key; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_api_key_key UNIQUE (api_key);


--
-- TOC entry 3219 (class 2606 OID 869485)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3221 (class 2606 OID 869487)
-- Name: visualizations visualizations_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.visualizations
    ADD CONSTRAINT visualizations_pkey PRIMARY KEY (id);


--
-- TOC entry 3224 (class 2606 OID 869489)
-- Name: widgets widgets_pkey; Type: CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.widgets
    ADD CONSTRAINT widgets_pkey PRIMARY KEY (id);


--
-- TOC entry 3158 (class 1259 OID 869490)
-- Name: alert_subscriptions_destination_id_alert_id; Type: INDEX; Schema: public; Owner: redash
--

CREATE UNIQUE INDEX alert_subscriptions_destination_id_alert_id ON public.alert_subscriptions USING btree (destination_id, alert_id);


--
-- TOC entry 3163 (class 1259 OID 869491)
-- Name: api_keys_object_type_object_id; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX api_keys_object_type_object_id ON public.api_keys USING btree (object_type, object_id);


--
-- TOC entry 3175 (class 1259 OID 869492)
-- Name: dashboard_groups_dashboard_id_idx; Type: INDEX; Schema: public; Owner: redash
--

CREATE UNIQUE INDEX dashboard_groups_dashboard_id_idx ON public.dashboard_groups USING btree (dashboard_id, group_id);


--
-- TOC entry 3185 (class 1259 OID 869493)
-- Name: data_sources_org_id_name; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX data_sources_org_id_name ON public.data_sources USING btree (org_id, name);


--
-- TOC entry 3171 (class 1259 OID 869494)
-- Name: idx_click_type; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX idx_click_type ON public.dashboard_clicks USING btree (click_type);


--
-- TOC entry 3172 (class 1259 OID 869495)
-- Name: idx_create_time; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX idx_create_time ON public.dashboard_clicks USING btree (create_time);


--
-- TOC entry 3173 (class 1259 OID 869496)
-- Name: idx_dashboard_clicks_id; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX idx_dashboard_clicks_id ON public.dashboard_clicks USING btree (dashboard_id);


--
-- TOC entry 3174 (class 1259 OID 869497)
-- Name: idx_user_id; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX idx_user_id ON public.dashboard_clicks USING btree (user_id);


--
-- TOC entry 3166 (class 1259 OID 869498)
-- Name: ix_api_keys_api_key; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_api_keys_api_key ON public.api_keys USING btree (api_key);


--
-- TOC entry 3180 (class 1259 OID 869499)
-- Name: ix_dashboards_is_archived; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_dashboards_is_archived ON public.dashboards USING btree (is_archived);


--
-- TOC entry 3181 (class 1259 OID 869500)
-- Name: ix_dashboards_is_draft; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_dashboards_is_draft ON public.dashboards USING btree (is_draft);


--
-- TOC entry 3182 (class 1259 OID 869501)
-- Name: ix_dashboards_slug; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_dashboards_slug ON public.dashboards USING btree (slug);


--
-- TOC entry 3203 (class 1259 OID 869502)
-- Name: ix_queries_is_archived; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_queries_is_archived ON public.queries USING btree (is_archived);


--
-- TOC entry 3204 (class 1259 OID 869503)
-- Name: ix_queries_is_draft; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_queries_is_draft ON public.queries USING btree (is_draft);


--
-- TOC entry 3205 (class 1259 OID 869504)
-- Name: ix_queries_search_vector; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_queries_search_vector ON public.queries USING gin (search_vector);


--
-- TOC entry 3208 (class 1259 OID 869505)
-- Name: ix_query_results_query_hash; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_query_results_query_hash ON public.query_results USING btree (query_hash);


--
-- TOC entry 3222 (class 1259 OID 869506)
-- Name: ix_widgets_dashboard_id; Type: INDEX; Schema: public; Owner: redash
--

CREATE INDEX ix_widgets_dashboard_id ON public.widgets USING btree (dashboard_id);


--
-- TOC entry 3196 (class 1259 OID 869507)
-- Name: notification_destinations_org_id_name; Type: INDEX; Schema: public; Owner: redash
--

CREATE UNIQUE INDEX notification_destinations_org_id_name ON public.notification_destinations USING btree (org_id, name);


--
-- TOC entry 3217 (class 1259 OID 869508)
-- Name: users_org_id_email; Type: INDEX; Schema: public; Owner: redash
--

CREATE UNIQUE INDEX users_org_id_email ON public.users USING btree (org_id, email);


--
-- TOC entry 3262 (class 2620 OID 869509)
-- Name: queries queries_search_vector_trigger; Type: TRIGGER; Schema: public; Owner: redash
--

CREATE TRIGGER queries_search_vector_trigger BEFORE INSERT OR UPDATE ON public.queries FOR EACH ROW EXECUTE FUNCTION public.queries_search_vector_update();


--
-- TOC entry 3225 (class 2606 OID 869510)
-- Name: access_permissions access_permissions_grantee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.access_permissions
    ADD CONSTRAINT access_permissions_grantee_id_fkey FOREIGN KEY (grantee_id) REFERENCES public.users(id);


--
-- TOC entry 3226 (class 2606 OID 869515)
-- Name: access_permissions access_permissions_grantor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.access_permissions
    ADD CONSTRAINT access_permissions_grantor_id_fkey FOREIGN KEY (grantor_id) REFERENCES public.users(id);


--
-- TOC entry 3227 (class 2606 OID 869520)
-- Name: alert_subscriptions alert_subscriptions_alert_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alert_subscriptions
    ADD CONSTRAINT alert_subscriptions_alert_id_fkey FOREIGN KEY (alert_id) REFERENCES public.alerts(id);


--
-- TOC entry 3228 (class 2606 OID 869525)
-- Name: alert_subscriptions alert_subscriptions_destination_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alert_subscriptions
    ADD CONSTRAINT alert_subscriptions_destination_id_fkey FOREIGN KEY (destination_id) REFERENCES public.notification_destinations(id);


--
-- TOC entry 3229 (class 2606 OID 869530)
-- Name: alert_subscriptions alert_subscriptions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alert_subscriptions
    ADD CONSTRAINT alert_subscriptions_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3230 (class 2606 OID 869535)
-- Name: alerts alerts_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alerts
    ADD CONSTRAINT alerts_query_id_fkey FOREIGN KEY (query_id) REFERENCES public.queries(id);


--
-- TOC entry 3231 (class 2606 OID 869540)
-- Name: alerts alerts_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.alerts
    ADD CONSTRAINT alerts_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3232 (class 2606 OID 869545)
-- Name: api_keys api_keys_created_by_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT api_keys_created_by_id_fkey FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- TOC entry 3233 (class 2606 OID 869550)
-- Name: api_keys api_keys_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.api_keys
    ADD CONSTRAINT api_keys_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3234 (class 2606 OID 869555)
-- Name: changes changes_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3237 (class 2606 OID 869560)
-- Name: dashboards dashboards_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboards
    ADD CONSTRAINT dashboards_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3238 (class 2606 OID 869565)
-- Name: dashboards dashboards_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboards
    ADD CONSTRAINT dashboards_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3239 (class 2606 OID 869570)
-- Name: data_source_groups data_source_groups_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_source_groups
    ADD CONSTRAINT data_source_groups_data_source_id_fkey FOREIGN KEY (data_source_id) REFERENCES public.data_sources(id);


--
-- TOC entry 3240 (class 2606 OID 869575)
-- Name: data_source_groups data_source_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_source_groups
    ADD CONSTRAINT data_source_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- TOC entry 3241 (class 2606 OID 869580)
-- Name: data_sources data_sources_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.data_sources
    ADD CONSTRAINT data_sources_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3242 (class 2606 OID 869585)
-- Name: events events_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3243 (class 2606 OID 869590)
-- Name: events events_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3244 (class 2606 OID 869595)
-- Name: favorites favorites_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.favorites
    ADD CONSTRAINT favorites_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3245 (class 2606 OID 869600)
-- Name: favorites favorites_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.favorites
    ADD CONSTRAINT favorites_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3235 (class 2606 OID 869605)
-- Name: dashboard_groups fk_dashboard_id; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboard_groups
    ADD CONSTRAINT fk_dashboard_id FOREIGN KEY (dashboard_id) REFERENCES public.dashboards(id);


--
-- TOC entry 3236 (class 2606 OID 869610)
-- Name: dashboard_groups fk_group_id; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.dashboard_groups
    ADD CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- TOC entry 3246 (class 2606 OID 869615)
-- Name: groups groups_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3247 (class 2606 OID 869620)
-- Name: notification_destinations notification_destinations_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.notification_destinations
    ADD CONSTRAINT notification_destinations_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3248 (class 2606 OID 869625)
-- Name: notification_destinations notification_destinations_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.notification_destinations
    ADD CONSTRAINT notification_destinations_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3249 (class 2606 OID 869630)
-- Name: queries queries_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_data_source_id_fkey FOREIGN KEY (data_source_id) REFERENCES public.data_sources(id);


--
-- TOC entry 3250 (class 2606 OID 869635)
-- Name: queries queries_last_modified_by_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_last_modified_by_id_fkey FOREIGN KEY (last_modified_by_id) REFERENCES public.users(id);


--
-- TOC entry 3251 (class 2606 OID 869640)
-- Name: queries queries_latest_query_data_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_latest_query_data_id_fkey FOREIGN KEY (latest_query_data_id) REFERENCES public.query_results(id);


--
-- TOC entry 3252 (class 2606 OID 869645)
-- Name: queries queries_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3253 (class 2606 OID 869650)
-- Name: queries queries_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3254 (class 2606 OID 869655)
-- Name: query_results query_results_data_source_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_results
    ADD CONSTRAINT query_results_data_source_id_fkey FOREIGN KEY (data_source_id) REFERENCES public.data_sources(id);


--
-- TOC entry 3255 (class 2606 OID 869660)
-- Name: query_results query_results_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_results
    ADD CONSTRAINT query_results_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3256 (class 2606 OID 869665)
-- Name: query_snippets query_snippets_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_snippets
    ADD CONSTRAINT query_snippets_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3257 (class 2606 OID 869670)
-- Name: query_snippets query_snippets_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.query_snippets
    ADD CONSTRAINT query_snippets_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3258 (class 2606 OID 869675)
-- Name: users users_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organizations(id);


--
-- TOC entry 3259 (class 2606 OID 869680)
-- Name: visualizations visualizations_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.visualizations
    ADD CONSTRAINT visualizations_query_id_fkey FOREIGN KEY (query_id) REFERENCES public.queries(id);


--
-- TOC entry 3260 (class 2606 OID 869685)
-- Name: widgets widgets_dashboard_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.widgets
    ADD CONSTRAINT widgets_dashboard_id_fkey FOREIGN KEY (dashboard_id) REFERENCES public.dashboards(id);


--
-- TOC entry 3261 (class 2606 OID 869690)
-- Name: widgets widgets_visualization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: redash
--

ALTER TABLE ONLY public.widgets
    ADD CONSTRAINT widgets_visualization_id_fkey FOREIGN KEY (visualization_id) REFERENCES public.visualizations(id);


-- Completed on 2021-12-22 15:51:28

--
-- PostgreSQL database dump complete
--

