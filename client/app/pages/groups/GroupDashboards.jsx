import { filter, map, includes, toLower } from "lodash";
import React from "react";
import Button from "antd/lib/button";
//import Dropdown from "antd/lib/dropdown";
//import Menu from "antd/lib/menu";
//import DownOutlinedIcon from "@ant-design/icons/DownOutlined";

import routeWithUserSession from "@/components/ApplicationArea/routeWithUserSession";
import navigateTo from "@/components/ApplicationArea/navigateTo";
import Paginator from "@/components/Paginator";

import { wrap as itemsList, ControllerType } from "@/components/items-list/ItemsList";
import { ResourceItemsSource } from "@/components/items-list/classes/ItemsSource";
import { StateStorage } from "@/components/items-list/classes/StateStorage";

import LoadingState from "@/components/items-list/components/LoadingState";
import ItemsTable, { Columns } from "@/components/items-list/components/ItemsTable";
import SelectItemsDialog from "@/components/SelectItemsDialog";
import { DashboardsPreviewCard } from "@/components/PreviewCard";

import GroupName from "@/components/groups/GroupName";
import ListItemAddon from "@/components/groups/ListItemAddon";
import Sidebar from "@/components/groups/DetailsPageSidebar";
import Layout from "@/components/layouts/ContentWithSidebar";
import wrapSettingsTab from "@/components/SettingsWrapper";

import notification from "@/services/notification";
import { currentUser } from "@/services/auth";
import Group from "@/services/group";
//import DataSource from "@/services/data-source";
import routes from "@/services/routes";

//import {DashboardService} from "@/services/dashboard";

class GroupDashboards extends React.Component {
  static propTypes = {
    controller: ControllerType.isRequired,
  };

  groupId = parseInt(this.props.controller.params.groupId, 10);

  group = null;

  sidebarMenu = [
    {
      key: "users",
      href: `groups/${this.groupId}`,
      title: "成员",
    },
    {
      key: "datasources",
      href: `groups/${this.groupId}/data_sources`,
      title: "数据源",
      isAvailable: () => currentUser.isAdmin,
    },
    {
      key: "dashboards",
      href: `groups/${this.groupId}/dashboards`,
      title: "报表",
      isAvailable: () => currentUser.isAdmin,
    },
  ];

  listColumns = [
    Columns.custom((text, item) => item.name, {
      title: "Name",
      field: "name",
      width: null,
    }),
    Columns.custom((text, item) => item.tags.join(), { title: "标签", width: null }),
    Columns.custom(
      (text, dashboards) => (
        <Button className="w-100" type="danger" onClick={() => this.removeGroupDashboards(dashboards)}>
        删除
        </Button>
      ),
      {
        width: "1%",
        isAvailable: () => currentUser.isAdmin,
      }
    ),
  ];

  componentDidMount() {
    Group.get({ id: this.groupId })
      .then(group => {
        this.group = group;
        this.forceUpdate();
      })
      .catch(error => {
        this.props.controller.handleError(error);
      });
  }

  removeGroupDashboards = dashboards => {
    Group.removeDashboards([{ group_id: dashboards.group_id, dashboard_id: dashboards.id }])
      .then(() => {
        notification.success('删除成功')
        this.props.controller.updatePagination({ page: 1 });
        this.props.controller.update();
      })
      .catch(() => {
        notification.error("删除报表失败");
      });
  };

  setDataSourcePermissions = (datasource, permission) => {
    const viewOnly = permission !== "full";

    Group.updateDataSource({ id: this.groupId, dataSourceId: datasource.id }, { view_only: viewOnly })
      .then(() => {
        datasource.view_only = viewOnly;
        this.forceUpdate();
      })
      .catch(() => {
        notification.error("Failed change data source permissions.");
      });
  };

  addDashboards = () => {
    const allDashboards = Group.newDashboards(this.groupId).then(res=>{
      res.results.forEach(item=>{
      if(Array.isArray(item.tags)){
        item.name = `【${item.tags.join()}】${item.name}`
      }
      
    });
    return Promise.resolve(res.results)});
    const alreadyAddedDashboards = map(this.props.controller.allItems, ds => ds.id);
    SelectItemsDialog.showModal({
      dialogTitle: "添加报表",
      inputPlaceholder: "搜索报表...",
      selectedItemsTitle: "已选报表",
      searchItems: searchTerm => {
        searchTerm = toLower(searchTerm);
        return allDashboards.then(items => filter(items, ds => includes(toLower(ds.name), searchTerm)));
      },
      renderItem: (item, { isSelected }) => {
        const alreadyInGroup = includes(alreadyAddedDashboards, item.id);
        return {
          content: (
            <DashboardsPreviewCard dashboards={item}>
              {<ListItemAddon isSelected={isSelected} alreadyInGroup={alreadyInGroup} />}
            </DashboardsPreviewCard>
          ),
          //isDisabled: alreadyInGroup,
          //className: isSelected || alreadyInGroup ? "selected" : "",
        };
      },
      renderStagedItem: (item, { isSelected }) => ({
        content: (
          <DashboardsPreviewCard dashboards={item}>
            <ListItemAddon isSelected={isSelected} isStaged />
          </DashboardsPreviewCard>
        ),
      }),
    }).onClose(items => {
      let data = items.map(item=>{return {group_id: item.group_id,dashboard_id: item.id}})
      const promises = Group.addDashboards(data);
      return promises.then(() => this.props.controller.update())
    });
  };

  render() {
    const { controller } = this.props;
    return (
      <div data-test="Group">
        <GroupName className="d-block m-t-0 m-b-15" group={this.group} onChange={() => this.forceUpdate()} />
        <Layout>
          <Layout.Sidebar>
            <Sidebar
              controller={controller}
              group={this.group}
              items={this.sidebarMenu}
              canAddDashboards={currentUser.isAdmin}
              onAddDashboardsClick={this.addDashboards}
              onGroupDeleted={() => navigateTo("groups")}
            />
          </Layout.Sidebar>
          <Layout.Content>
            {!controller.isLoaded && <LoadingState className="" />}
            {controller.isLoaded && controller.isEmpty && (
              <div className="text-center">
                <p>该角色没有添加报表。</p>
                {currentUser.isAdmin && (
                  <Button type="primary" onClick={this.addDashboards}>
                    <i className="fa fa-plus m-r-5" />
                    添加报表
                  </Button>
                )}
              </div>
            )}
            {controller.isLoaded && !controller.isEmpty && (
              <div className="table-responsive">
                <ItemsTable
                  items={controller.pageItems}
                  columns={this.listColumns}
                  showHeader={false}
                  context={this.actions}
                  orderByField={controller.orderByField}
                  orderByReverse={controller.orderByReverse}
                  toggleSorting={controller.toggleSorting}
                />
                <Paginator
                  showPageSizeSelect
                  totalCount={controller.totalItemsCount}
                  pageSize={controller.itemsPerPage}
                  onPageSizeChange={itemsPerPage => controller.updatePagination({ itemsPerPage })}
                  page={controller.page}
                  onChange={page => controller.updatePagination({ page })}
                />
              </div>
            )}
          </Layout.Content>
        </Layout>
      </div>
    );
  }
}

const GroupDashboardsPage = wrapSettingsTab(
  "Groups.Dashboards",
  null,
  itemsList(
    GroupDashboards,
    () =>
      new ResourceItemsSource({
        isPlainList: true,
        getRequest(unused, { params: { groupId } }) {
          return { id: groupId };
        },
        getResource() {
          return Group.dashboards.bind(Group);
        },
      }),
    () => new StateStorage({ orderByField: "name" })
  )
);

routes.register(
  "Groups.Dashboards",
  routeWithUserSession({
    path: "/groups/:groupId/dashboards",
    title: "角色报表",
    render: pageProps => <GroupDashboardsPage {...pageProps} currentPage="dashboards" />,
  })
);
