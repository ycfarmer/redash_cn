import React from "react";
import Alert from "antd/lib/alert";
import Form from "antd/lib/form";
import Checkbox from "antd/lib/checkbox";
import Tooltip from "antd/lib/tooltip";
import Skeleton from "antd/lib/skeleton";
import DynamicComponent from "@/components/DynamicComponent";
import { clientConfig } from "@/services/auth";
import { SettingsEditorPropTypes, SettingsEditorDefaultProps } from "../prop-types";

export default function PasswordLoginSettings(props) {
  const { settings, values, onChange, loading } = props;

  const isTheOnlyAuthMethod =
    !clientConfig.googleLoginEnabled && !clientConfig.ldapLoginEnabled && !values.auth_saml_enabled;

  return (
    <DynamicComponent name="OrganizationSettings.PasswordLoginSettings" {...props}>
      {!loading && !settings.auth_password_login_enabled && (
        <Alert
          message="基于密码的登录当前已禁用，用户将只能在启用SSO选项的情况下登录."
          type="warning"
          className="m-t-15 m-b-15"
        />
      )}
      <Form.Item label="密码登录">
        {loading ? (
          <Skeleton title={{ width: 300 }} paragraph={false} active />
        ) : (
          <Checkbox
            checked={values.auth_password_login_enabled}
            disabled={isTheOnlyAuthMethod}
            onChange={e => onChange({ auth_password_login_enabled: e.target.checked })}>
            <Tooltip
              title={
                isTheOnlyAuthMethod ? "只有用户启用了其它登录认证，才可以取消用户名密码登录方式." : null
              }
              placement="right">
              启用用户名密码登录方式
            </Tooltip>
          </Checkbox>
        )}
      </Form.Item>
    </DynamicComponent>
  );
}

PasswordLoginSettings.propTypes = SettingsEditorPropTypes;

PasswordLoginSettings.defaultProps = SettingsEditorDefaultProps;
