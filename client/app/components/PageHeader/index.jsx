import React from "react";
import PropTypes from "prop-types";

import "./index.less";

export default function PageHeader({ title, actions,className="page-header-wrapper" }) {
  return (
    <div className={className}>
      <h3>{title}</h3>
      {actions && <div className="page-header-actions">{actions}</div>}
    </div>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string,
  actions: PropTypes.node,
};

PageHeader.defaultProps = {
  title: "",
  actions: null,
};
