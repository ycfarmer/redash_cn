import React from "react";
import ReactDOM from "react-dom";

import "@/config";

import ApplicationArea from "@/components/ApplicationArea";
import offlineListener from "@/services/offline-listener";
import zhCN from 'antd/lib/locale/zh_CN';
import ConfigProvider from 'antd/lib/config-provider';
ReactDOM.render(<ConfigProvider locale={zhCN}>
                  <ApplicationArea />
                </ConfigProvider>, document.getElementById("application-root"), () => {
                  offlineListener.init();
                });
