import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime, UniqueConstraint,Index,Float


Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer,primary_key=True)
    name = Column(String(128),index=True,nullable=True,unique=True)
    create_time = Column(DateTime,default=datetime.datetime.now)

    __table_args__ = (
        UniqueConstraint('id','name',name='uni_id_name'),
        Index('idx_id_name','name','create_time')
    )


def create_table():
    engine = create_engine(
        "mysql+pymysql://root:",
    )


